#!/usr/bin/bash

USER="jxadmin"
GROUP="wasadmin"
ROLE=__ROLE__
WAS_VER="85"
CELL_SUFFIX=__CELL__
IM_INSTALL_DIR="/opt/IBM"
IM_VERSION="agent.installer.linux.gtk.x86_64_1.6.2000.20130301_2248.zip"
WAS_INSTALL_DIR="/usr/websphere${WAS_VER:0:1}"
NDB_NAME="ndb${WAS_VER}_${CELL_SUFFIX}"
WAS_NAME="was${WAS_VER}_${CELL_SUFFIX}"
IHS_NAME="ihs${WAS_VER}_${CELL_SUFFIX}"
PLG_NAME="plg${WAS_VER}_${CELL_SUFFIX}"
CELL_NAME=${WAS_NAME}

SOFTWARE="compat-libstdc++-33.i686 compat-libstdc++-33.x86_64 gtk2.i686 libXtst.i686 unzip xauth liberation-fonts-common urw-fonts.noarch libaio bc gtk2 libXtst lsof net-tools unzip"

yum update -y
yum install -y ${SOFTWARE}

mkdir -p ${IM_INSTALL_DIR}/installer ${IM_INSTALL_DIR}/InstallationManagerData
mkdir -p "${WAS_INSTALL_DIR}/${NDB_NAME}"

# create a swap file
dd if=/dev/zero of=/swapfile-additional bs=1M count=4048
mkswap /swapfile-additional
chmod 600 /swapfile-additional
echo "/swapfile-additional swap swap    0   0" >> /etc/fstab
mount -a
swapon -a
swapon -s

# Websphere user
echo ${USER}:${USERPASSWORD} > /tmp/users
groupadd ${GROUP}
useradd -m -g ${GROUP} ${USER}
chpasswd < /tmp/users
rm /tmp/users

cd ~root
curl -O http://primergy/software/ibm/websphere/${IM_VERSION} \
     -O http://primergy/software/ibm/java/ibm-java-x86_64-sdk-8.0-5.11.bin \
     -O http://primergy/software/ibm/websphere/was.repo.8550.ndtrial_part1.zip \
     -O http://primergy/software/ibm/websphere/was.repo.8550.ndtrial_part2.zip \
     -O http://primergy/software/ibm/websphere/was.repo.8550.ndtrial_part3.zip

mv ./${IM_VERSION} ${IM_INSTALL_DIR}/installer
mv was.repo*.zip ${IM_INSTALL_DIR}/InstallationManagerData

cd ${IM_INSTALL_DIR}/installer
unzip ./${IM_VERSION} && ./installc -acceptLicense

cd ${IM_INSTALL_DIR}/InstallationManagerData
for each in $(ls *.zip)
do
  unzip $each
done

${IM_INSTALL_DIR}/InstallationManager/eclipse/tools/imcl install com.ibm.websphere.NDTRIAL.v85_8.5.5000.20130514_1044 -installationDirectory ${WAS_INSTALL_DIR}/${NDB_NAME}/  -repositories ${IM_INSTALL_DIR
}/InstallationManagerData/repository.config -acceptLicense

if [[ $ROLE == "dmgr" ]]; then
  mkdir -p ${WAS_INSTALL_DIR}/${WAS_NAME}/DeploymentManager
  cd ${WAS_INSTALL_DIR}/${NDB_NAME}/bin
  ./manageprofiles.sh -create -profileName ${CELL_NAME}_dm -profilePath ${WAS_INSTALL_DIR}/${WAS_NAME}/DeploymentManager -templatePath ../profileTemplates/management
  chown -R ${USER}:${GROUP} ${WAS_INSTALL_DIR}/${WAS_NAME}
  su - ${USER} -c  ${WAS_INSTALL_DIR}/${WAS_NAME}/DeploymentManager/bin/startManager.sh
fi

if [[ $ROLE == "node" ]]; then
  mkdir -p ${WAS_INSTALL_DIR}/${WAS_NAME}/AppServer
  cd ${WAS_INSTALL_DIR}/${NDB_NAME}/bin
  ./manageprofiles.sh -create -profileName ${CELL_NAME} -profilePath ${WAS_INSTALL_DIR}/${WAS_NAME}/AppServer
  chown -R ${USER}:${GROUP} ${WAS_INSTALL_DIR}/${WAS_NAME}
  su - ${USER} -c  ${WAS_INSTALL_DIR}/${WAS_NAME}/AppServer/bin/addNode.sh __DMGR__
fi

